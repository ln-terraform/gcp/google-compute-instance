resource "google_compute_instance" "instance" {
  project             = var.project
  name                = var.name
  machine_type        = var.machine_type
  zone                = var.zone
  tags                = var.tags
  can_ip_forward      = var.can_ip_forward
  deletion_protection = var.deletion_protection

  boot_disk {
    initialize_params {
      image = var.image_type
      size  = var.disk_size
      type  = var.disk_type
    }
  }

  dynamic "network_interface" {
    for_each = var.networks
    content {
      network            = lookup(network_interface.value, "network", "default")
      subnetwork         = lookup(network_interface.value, "subnetwork", null)
      subnetwork_project = lookup(network_interface.value, "subnetwork_project", null)
      network_ip         = lookup(network_interface.value, "network_ip", null)

      dynamic "access_config" {
        for_each = lookup(network_interface.value, "allow_internet_access", false) ? [{}] : []

        content {
          nat_ip                 = lookup(network_interface.value, "nat_ip", "")
          public_ptr_domain_name = lookup(network_interface.value, "public_ptr_domain_name", "")
          network_tier           = lookup(network_interface.value, "network_tier", "STANDARD")
        }
      }
    }
  }

  dynamic "attached_disk" {
    for_each = var.attached_disks
    content {
      source      = attached_disk.value["source"]
      device_name = attached_disk.value["device_name"]
      mode        = attached_disk.value["mode"]
    }
  }

  scheduling {
    preemptible       = lookup(var.scheduling, "preemptible", true)
    automatic_restart = lookup(var.scheduling, "automatic_restart", true)
  }

  metadata_startup_script = var.metadata_startup_script
}