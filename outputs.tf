output "name" {
  value = google_compute_instance.instance.name
}

output "id" {
  value = google_compute_instance.instance.id
}

output "selflink" {
  value = google_compute_instance.instance.self_link
}

output "instance_id" {
  value = google_compute_instance.instance.instance_id
}