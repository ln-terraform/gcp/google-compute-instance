variable "project" {
  type = string
}

variable "name" {
  type = string
}

variable "machine_type" {
  type = string
}

variable "zone" {
  type = string
}

variable "tags" {
  type    = list(string)
  default = []
}

variable "image_type" {
  type    = string
  default = "ubuntu-os-cloud/ubuntu-minimal-2004-lts"
}

variable "disk_type" {
  type    = string
  default = "pd-standard"
}

variable "disk_size" {
  type = number
}

variable "can_ip_forward" {
  type    = bool
  default = false
}

variable "deletion_protection" {
  type    = bool
  default = false
}

variable "networks" {
  type = list(map(string))
  default = [{
    network               = "default"
    allow_internet_access = false
  }]
}

variable "scheduling" {
  type    = map(string)
  default = {}
}

variable "enable_internet_access" {
  type    = bool
  default = false
}

variable "access_config" {
  type    = map(string)
  default = {}
}

variable "attached_disks" {
  type = list(object({
    source      = string
    device_name = string
    mode        = string
  }))
  default = []
}

variable "metadata_startup_script" {
  type    = string
  default = null
}